package main

import (
	"fmt"
	"math"
)

func sqrt(x float64) float64 {
	const DeltaLim = 1e-7
	z := 1.0
	for delta := z; math.Abs(delta) > DeltaLim; {
		delta = (z*z - x) / (2 * z)
		z -= delta
		fmt.Println(z, delta)
	}
	return z
}

func main() {
	fmt.Println(sqrt(4))
}

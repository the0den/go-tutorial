package main

import (
	"io"
	"os"
	"strings"
)

type rot13Reader struct {
	r io.Reader
}

func (r13r rot13Reader) Read(b []byte) (int, error) {
	const LastUp = 'Z'
	const LastLo = 'z'
	limCorrected := func(v0, v1, lim byte) byte {
		if v0 <= lim && v1 > lim {
			return v1 - 26
		}
		return v1
	}

	l, e := r13r.r.Read(b)
	for i := 0; i < l; i++ {
		curByte := b[i]
		if curByte < 'A' || curByte > 'z' {
			continue
		}
		next := curByte + 13
		b[i] = limCorrected(curByte, next, LastUp)
		b[i] = limCorrected(curByte, next, LastLo)
	}
	return l, e
}

func main() {
	s := strings.NewReader("Lbh penpxrq gur pbqr!")
	r := rot13Reader{s}
	io.Copy(os.Stdout, &r)
}

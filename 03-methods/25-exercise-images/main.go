package main

import (
	"image"
	"image/color"

	"golang.org/x/tour/pic"
)

type Canvas [][]color.RGBA
type CanvasRow []color.RGBA

func Pic(dx, dy int) Canvas {
	result := make(Canvas, dy)
	for y := range result {
		result[y] = make(CanvasRow, dx)
		for x := range result[y] {
			v := uint8((x + y) / 2)
			result[y][x] = color.RGBA{v, v, 255, 255}
		}
	}
	return result
}

type Image struct {
	canvas Canvas
}

// ColorModel returns the Image's color model.
func (i *Image) ColorModel() color.Model {
	return color.RGBAModel
}

// Bounds returns the domain for which At can return non-zero color.
// The bounds do not necessarily contain the point (0, 0).
func (i *Image) Bounds() image.Rectangle {
	return image.Rect(0, 0, len(i.canvas[0]), len(i.canvas))
}

// At returns the color of the pixel at (x, y).
// At(Bounds().Min.X, Bounds().Min.Y) returns the upper-left pixel of the grid.
// At(Bounds().Max.X-1, Bounds().Max.Y-1) returns the lower-right one.
func (i *Image) At(x, y int) color.Color {
	return i.canvas[y][x]
}

func main() {
	m := Image{}
	m.canvas = Pic(300, 200)
	pic.ShowImage(&m)
}
